package tests;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import network.Control;

public class ConnectionTest {
	static Socket sc;
	static ObjectOutputStream out;
	static ObjectInputStream in;
	
	public static void main(String[] args) {
		try {
			sc = new Socket("127.0.0.1",4455);
			System.out.println("Open new socket");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			out = new ObjectOutputStream(sc.getOutputStream());
			System.out.println("Open output stream");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			in = new ObjectInputStream(sc.getInputStream());
			System.out.println("Open input stream");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Control cmd =new Control();
		cmd.setCommand(Control.CLOSE);
		try {
			out.writeObject(cmd);
			System.out.println("Write control object");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
