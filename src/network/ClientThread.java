package network;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import gui.observer.HomeObserver;
import network.object.NetNote;

public class ClientThread extends Thread {
	private Socket clientSocket = null;
	private Control command = null;
	private ObjectInputStream obIn;
	private ObjectOutputStream obOut;
	private HomeObserver homeObserver;

	public ClientThread(Socket clientSocket) {
		this.clientSocket = clientSocket;
		this.homeObserver = HomeObserver.getHomeObserver();
		System.out.println("<<New Client Thread >>");
		try {
			this.obIn = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
			this.obOut = new ObjectOutputStream(clientSocket.getOutputStream());
			// System.out.println("<<New Client Thread init stream >>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void run() {
		// System.out.println("<<Start New Client Thread run method>>");
		while (!this.clientSocket.isClosed()) {

			try {
				command = (Control) obIn.readObject();
				executeCommand();
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} // glowna petla

		System.out.println("<<Stop New Client Thread>>");
	}// end run

	private void executeCommand() {
		if (this.command.getCommand().equals(Control.CLOSE)) {
			closeConnection();
		} /// Zamknij
		else if (this.command.getCommand().equals(Control.READ_SENSORS)) {
			sendSensorState();
		} else if (this.command.getCommand().equals(Control.READ_CONTROLS)) {
			sendControl();
		} else if (this.command.getCommand().equals(Control.SWITCH_LIGHT)) {
			switchLight();
		} else if (this.command.getCommand().equals(Control.SWITCH_RADIATOR)) {
			switchRadiator();
		} else if (this.command.getCommand().equals(Control.ADJUST_RADIATOR)) {
			adjustRadiator();
		} else if (this.command.getCommand().equals(Control.READ_NOTES)) {
			sendNotes();
		}else if (this.command.getCommand().equals(Control.ADD_NOTE)) {
			addNote();
		}

	}// end execute command method

	private synchronized void closeConnection() {

		try {
			this.obIn.close();
			this.obOut.close();
			this.clientSocket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Close conection with  : " + clientSocket.getRemoteSocketAddress().toString() + ">>");
	}

	private synchronized void sendSensorState() {
		try {
			this.obOut.writeObject(homeObserver.getSensorAL());
			this.obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Read Sensor Command>>");
	}

	private synchronized void sendControl() {
		try {
			this.obOut.writeObject(homeObserver.getControlAL());
			this.obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Read Control Sensor Command>>");
	}

	private void switchLight() {

		try {
			String name = this.obIn.readUTF();
			homeObserver.switchLightcontrol(name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
					}
		System.out.println("<<Switch Light Command>>");
	}

	private void switchRadiator() {

		try {
			String name = this.obIn.readUTF();
			homeObserver.switchRadiatorControl(name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Switch Radiator Command>>");
	}

	private void adjustRadiator() {

		try {
			String name = this.obIn.readUTF();
			// System.out.println(">>>>>>>>" +name);
			double temp = this.obIn.readDouble();
			// System.out.println(">>>>>>>>" +temp);
			homeObserver.adjustRadiatorControl(name, temp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Adjust Radiator Command>>");
	}

	private synchronized void sendNotes() {
		try {
			this.obOut.writeObject(homeObserver.getNoteslAL());
			this.obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Read Note Command>>");
	}

	private synchronized void addNote() {
		try {
			NetNote note = (NetNote) this.obIn.readObject();
			homeObserver.getMainApp().addNote(note.getAuthor(), note.getContent());
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("<<Add Note Command>>");
	}

}// end 
