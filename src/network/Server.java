package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server extends Thread {
	private static ServerSocket serverSocket;
	private static Socket clientSocket;
	private static ExecutorService serverExecutor;
	private boolean work = true;
	private int port; 

	public Server(int port) {
		this.port=port;
		serverExecutor = Executors.newCachedThreadPool();
	}

	public void shutdownServer() {
		this.work = false;
	}

	@Override
	public void run() {

		try{
		serverSocket = new ServerSocket(port);
		serverSocket.setSoTimeout(60000);
		System.out.println("<Server Start>");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		while (this.work) {
			serverTask();
		}
		System.out.println("<Server Shutdown>");
		serverExecutor.shutdown();
	}

	private void serverTask() {
		try {
			clientSocket = serverSocket.accept();
				System.out.println("<New user connect : " + clientSocket.getRemoteSocketAddress().toString()+">");
			serverExecutor.execute(new ClientThread(clientSocket));
			//	new ClientThread(clientSocket).start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("<No incoming connection>");
		}

	}

}
