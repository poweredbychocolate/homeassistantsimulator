package network.object;

import java.io.Serializable;

import network.inteface.NetSensor;


public class NetDoorSensor implements NetSensor, Serializable {
	private String name;
	private boolean state;
	
	public NetDoorSensor(String name,Boolean state) {
		this.name = name;
		this.state = state;		
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void setName(String name) {
		this.name = name;

	}

	public void setState(boolean state) {
		this.state = state;

	}

	public boolean isActive() {
		return this.state;
	}

	@Override
	public String getName() {
		return this.name;
	}


	public boolean getState() {
		// TODO Auto-generated method stub
		return this.state;
	}

}
