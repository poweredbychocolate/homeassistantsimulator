package network.object;

import java.io.Serializable;

import network.inteface.NetControl;

public class NetRadiatorControl implements NetControl,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private double temperature;
	private boolean heating;
	
	public NetRadiatorControl() {
		// TODO Auto-generated constructor stub
	}
	public NetRadiatorControl(String name,double temperature,boolean isHeating) {
	this.name=name;
	this.temperature=temperature;
	this.heating=isHeating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public boolean isHeating() {
		return heating;
	}

	public void setHeating(boolean heating) {
		this.heating = heating;
	}

}
