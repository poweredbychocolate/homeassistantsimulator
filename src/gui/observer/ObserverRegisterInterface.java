package gui.observer;

public interface ObserverRegisterInterface {
	public void registerObserver(HomeObserver ho);
}
