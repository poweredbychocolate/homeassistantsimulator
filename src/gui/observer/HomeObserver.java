package gui.observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import gui.MainApp;
import network.inteface.NetControl;
import network.inteface.NetSensor;
import network.object.NetDoorSensor;
import network.object.NetLightControl;
import network.object.NetNote;
import network.object.NetRadiatorControl;
import network.object.NetRainSensor;
import network.object.NetTemperatureSensor;

//observer class
public class HomeObserver {
	private static HomeObserver hob;
	private MainApp mainApp;
	private HashMap<String, NetSensor> sensorHM;
	private HashMap<String, NetControl> controlHM;
	private ArrayList<NetNote> notesAL;
	

	private HomeObserver() {
		sensorHM= new HashMap<String, NetSensor>(10);
		controlHM = new HashMap<String, NetControl>(10);
		notesAL = new ArrayList<NetNote>(10);
	}

	public static HomeObserver getHomeObserver() {
		if (hob == null)
			hob = new HomeObserver();
		return hob;
	}
	public void observeMainApp(MainApp mainApp)
	{
		this.mainApp=mainApp;
	}
	public MainApp getMainApp()
	{
		return this.mainApp;
	}

	/// netSensor
	public HashMap<String, NetSensor> getSensorHM() {
		return this.sensorHM;
	}
	public ArrayList<NetSensor> getSensorAL() {
		return new ArrayList<NetSensor>(this.sensorHM.values());
	}
	//door net sensor
	public void addDoorSensor(NetDoorSensor doorSensor) {
		this.sensorHM.put(doorSensor.getName(), doorSensor);
		
	}

	public void updateDoorSensor(String name, boolean state) {		
		((NetDoorSensor)this.sensorHM.get(name)).setState(state);
		
	}
	//rain net sensor 
	public void addRainSensor(NetRainSensor rainSensor) {
		this.sensorHM.put(rainSensor.getName(), rainSensor);
		
	}

	public void updateRainSensor(String name, boolean state) {		
		((NetRainSensor)this.sensorHM.get(name)).setState(state);
		
	}

	/// netTemperatureSensor

	public void addTemeperatureSensor(NetTemperatureSensor sensor) {
		this.sensorHM.put(sensor.getName(), sensor);
		// System.out.println("[Home Observer Sensor Count] " +sensorHM.size());
	}

	public void updateTemperatureSensor(String name, double value) {
		((NetTemperatureSensor)this.sensorHM.get(name)).setValue(value);

	}
	/// netControl 
	public HashMap<String, NetControl> getControlHM() {
		return this.controlHM;
	}
	public ArrayList<NetControl> getControlAL() {
		return new ArrayList<NetControl>(this.controlHM.values());
	}
	///LightControl
	public void addLightControl(String name, boolean state) {
		this.controlHM.put(name,new NetLightControl(name, state));
	}

	public void switchLightcontrol(String name) {
		((NetLightControl)this.controlHM.get(name)).setState(!((NetLightControl)this.controlHM.get(name)).getState());
		 //System.out.println(this.controlHM.get(name).getName()+" :: "+((NetLightControl)this.controlHM.get(name)).getState());
		if(mainApp!=null) mainApp.switchLight(name);
	}
	///RadiatorControl
	public void addRadiatorControl(String name,double temperature, boolean isHeating) {
		this.controlHM.put(name,new NetRadiatorControl(name, temperature, isHeating));
	}

	public void switchRadiatorControl(String name) {
		((NetRadiatorControl)this.controlHM.get(name)).setHeating(!((NetRadiatorControl)this.controlHM.get(name)).isHeating());
		 //System.out.println(this.controlHM.get(name).getName()+" :: "+((NetLightControl)this.controlHM.get(name)).getState());
		if(mainApp!=null) mainApp.switchRadiator(name);
	}
	public void adjustRadiatorControl(String name,double temperature) {
		((NetRadiatorControl)this.controlHM.get(name)).setTemperature(temperature);
		 //System.out.println(this.controlHM.get(name).getName()+" :: "+((NetLightControl)this.controlHM.get(name)).getState());
		if(mainApp!=null) mainApp.adjustRadiator(name, temperature);
	}
	///Notes
	public ArrayList<NetNote> getNoteslAL() {
		//reverse notes list newest notes is first
		ArrayList<NetNote> tmpList = new ArrayList<NetNote>(this.notesAL);
		Collections.reverse(tmpList);
		return tmpList;
	}
	public void addNotes(String author, String content) {
		this.notesAL.add(new NetNote(author, content));
		//System.out.println(notesAL.size());
		//if(mainApp!=null) mainApp.addNote(author, content);
	}
}
