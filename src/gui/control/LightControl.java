package gui.control;

import gui.Icons;
import gui.observer.HomeObserver;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class LightControl extends GridPane{
	private boolean isLighting=false;
	private Label nameLabel;
	private Label iconLabel;
	private ImageView images[];
	private Icons ic;
	private int lightState=0;
	
	public LightControl(String name,Boolean isLighting) {
		///
		this.ic = Icons.enableIcons();
		this.nameLabel=new Label(name);
		this.isLighting=isLighting;
		this.iconLabel = new Label();
		////
		this.images = new ImageView[2];
		this.images[0]=ic.getBulbOnIcon();
		this.images[1]=ic.getBulbOffIcon();
		if(this.isLighting) this.lightState=0;
		else  this.lightState=1;
		this.iconLabel.setGraphic(images[this.lightState]);
		
		////
		this.add(iconLabel, 0,0,2,1);
		this.add(nameLabel, 0, 1);
		HomeObserver.getHomeObserver().addLightControl(getName(),getState());
		}

public boolean isLighting()
{
	return this.isLighting;
}
public boolean getState()
{
	return this.isLighting;
}
public void switchLightState()
{
	this.lightState=++this.lightState%2;
	this.isLighting=!this.isLighting;
	this.iconLabel.setGraphic(images[this.lightState]);
	//HomeObserver.getHomeObserver().updateSwitchSensor(this.nameLabel.getText(), this.isOpened);
}
public String getName() {
	return nameLabel.getText();
}

}
