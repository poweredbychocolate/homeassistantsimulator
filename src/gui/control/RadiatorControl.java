package gui.control;

import gui.Icons;
import gui.MainApp;
import gui.observer.HomeObserver;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;


public class RadiatorControl extends GridPane {
	private double temp = 0.0;
	private Label nameLabel, tempLabel, imageLabel;
	private Icons ic;
	private ImageView images[];
	private int radiatorState = 0;

	public RadiatorControl(String name, double temp) {
		this.ic = Icons.enableIcons();
		this.setVgap(10.0);
		this.setHgap(10.0);
		this.temp = temp;
		////
		this.tempLabel = new Label(Double.toString(temp) + " "+MainApp.SYMBOLS_CELSIUS);
		this.tempLabel.setTextAlignment(TextAlignment.RIGHT);
		this.nameLabel = new Label(name);
		this.imageLabel = new Label();
		////
		this.images = new ImageView[2];
		this.images[0] = ic.getRadiatorOffIcon();
		this.images[1] = ic.getRadiatorOnIcon();
		this.radiatorState = 0;
		this.imageLabel.setGraphic(images[this.radiatorState]);
		///
		this.add(imageLabel, 0, 0, 3, 1);
		this.add(nameLabel, 0, 1, 2, 1);
		this.add(tempLabel, 2, 1);

		///
		
	HomeObserver.getHomeObserver().addRadiatorControl(getName(), getTemperature(), isHeating());
	}

	public double getTemperature() {
		return this.temp;
	}

	public String getName() {
		return nameLabel.getText();
	}
	public boolean isHeating()
	{
		return this.radiatorState==0 ? false : true;
	}
	public void changeTemperature(double val) {
		this.temp = val;
		this.tempLabel.setText(String.format("%3.2f", val) + MainApp.SYMBOLS_CELSIUS);
		//warning loop witch ClientThread
	//	HomeObserver.getHomeObserver().adjustRadiatorControl(getName(), val);
	}
	public void changeHeating()
	{
		this.radiatorState=++this.radiatorState%2;
		this.imageLabel.setGraphic(images[this.radiatorState]);
	//	HomeObserver.getHomeObserver().switchRadiatorControl(getName());
	}
}
