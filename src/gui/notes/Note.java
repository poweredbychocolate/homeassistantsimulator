package gui.notes;


import gui.observer.HomeObserver;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class Note extends GridPane {
	private Label author;
	private TextArea content;

	public Note() {
		this.author = new Label();
		this.author.setPadding(new Insets(2,10,2,10));
		this.content = new TextArea();
		this.content.setEditable(false);
		this.content.setWrapText(true);
		this.setMaxWidth(200);
		this.add(this.author, 0, 0);
		this.add(this.content, 0, 1,1,2);
		this.setBorder(new Border(new BorderStroke(Color.ORANGE, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));
		this.setPadding(new Insets(10,0, 15, 0));
	}

	public Note(String author, String text) {
		this();
		setAuthor(author);
		setText(text);
		HomeObserver.getHomeObserver().addNotes(author, text);
		
	}

	public String getAuthor() {
		return author.getText();
	}

	public void setAuthor(String author) {
		this.author.setText(author);
	}

	public String getText() {
		return content.getText();
	}

	public void setText(String text) {
		this.content.setText(text);
	}

}
