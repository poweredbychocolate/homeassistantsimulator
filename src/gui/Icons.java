package gui;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Icons {
	private static Icons icons=null;
	private Image thermometerIcon,openedDoor,closedDoor,sunIcon,rainIcon,bulbOn,bulbOff,ledDiode,radiatorOn,radiatorOff;
	private Icons() {
		this.thermometerIcon = new Image("thermometer.png");
		this.openedDoor = new Image("opened-door.png");
		this.closedDoor = new Image("closed-door.png");
		this.sunIcon = new Image("sun.png");
		this.rainIcon = new Image("rain.png");
		////part 2
		this.bulbOn = new Image("bulb-light.png");
		this.bulbOff = new Image("bulb-no.png");
		this.ledDiode = new Image("diode.png");
		this.radiatorOn = new Image("radiator-on.png");
		this.radiatorOff = new Image("radiator-off.png");
		
	}
	public static Icons enableIcons()
	{
		if(icons==null) icons= new Icons();
		return icons;
	}
public ImageView getThermometerIcon() {
	return new ImageView(this.thermometerIcon);
}
public ImageView getOpenedDoorIcon() {
	return new ImageView(this.openedDoor);
}
public ImageView getClosedDoorIcon() {
	return new ImageView(this.closedDoor);
}
public ImageView getSunIcon() {
	return new ImageView(this.sunIcon);
}
public ImageView getRainIcon() {
	return new ImageView(this.rainIcon);
}
////part 2
public ImageView getBulbOnIcon() {
	return new ImageView(this.bulbOn);
}
public ImageView getBulbOffIcon() {
	return new ImageView(this.bulbOff);
}
public ImageView getLEDDiodeIcon() {
	return new ImageView(this.ledDiode);
}
public ImageView getRadiatorOnIcon() {
	return new ImageView(this.radiatorOn);
}
public ImageView getRadiatorOffIcon() {
	return new ImageView(this.radiatorOff);
}
}
