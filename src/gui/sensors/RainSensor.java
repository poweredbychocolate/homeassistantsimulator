package gui.sensors;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import network.object.NetRainSensor;
import gui.Icons;
import gui.observer.HomeObserver;

public class RainSensor extends GridPane{
	private boolean isRain=false;
	private Label nameLabel;
	private Button iconButton;
	private ImageView images[];
	private Icons ic;
	private int rainState=0;
	
	public RainSensor (String name,Boolean isRain) {
		///
		this.ic = Icons.enableIcons();
		this.nameLabel=new Label(name);
		this.isRain=isRain;
		this.iconButton = new Button();
		////
		this.images = new ImageView[2];
		this.images[0]=ic.getSunIcon();
		this.images[1]=ic.getRainIcon();
		if(this.isRain) this.rainState=1;
		else  this.rainState=0;
		this.iconButton.setGraphic(images[this.rainState]);
		this.iconButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				// TODO Auto-generated method stub
				switchRainState();
			}
			
		});
		////
		this.add(iconButton, 0,0,2,1);
		this.add(nameLabel, 0, 1);
		HomeObserver.getHomeObserver().addRainSensor(new NetRainSensor(this.getName(),this.getState()));
	}
public boolean isRain()
{
	return this.isRain;
}
public boolean getState()
{
	return this.isRain;
}
public String getName()
{
	return this.nameLabel.getText();
}
private void switchRainState()
{
	this.rainState=++this.rainState%2;
	this.isRain=!this.isRain;
	this.iconButton.setGraphic(images[this.rainState]);
	HomeObserver.getHomeObserver().updateRainSensor(getName(), getState());
}
}
