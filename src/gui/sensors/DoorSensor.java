package gui.sensors;

import gui.Icons;
import gui.observer.HomeObserver;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import network.object.NetDoorSensor;

public class DoorSensor extends GridPane{
	private boolean isOpened=false;
	private Label nameLabel;
	private Button iconButton;
	private ImageView images[];
	private Icons ic;
	private int doorState=0;
	
	public DoorSensor(String name,Boolean isOpened) {
		///
		this.ic = Icons.enableIcons();
		this.nameLabel=new Label(name);
		this.isOpened=isOpened;
		this.iconButton = new Button();
		////
		this.images = new ImageView[2];
		this.images[0]=ic.getOpenedDoorIcon();
		this.images[1]=ic.getClosedDoorIcon();
		if(this.isOpened) this.doorState=0;
		else  this.doorState=1;
		this.iconButton.setGraphic(images[this.doorState]);
		this.iconButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				// TODO Auto-generated method stub
				switchDoorState();
			}
			
		});
		////
		this.add(iconButton, 0,0,2,1);
		this.add(nameLabel, 0, 1);
		HomeObserver.getHomeObserver().addDoorSensor(new NetDoorSensor(this.getName(),this.getState()));
	}
public boolean isOpened()
{
	return this.isOpened;
}
public boolean getState()
{
	return this.isOpened;
}
private void switchDoorState()
{
	this.doorState=++this.doorState%2;
	this.isOpened=!this.isOpened;
	this.iconButton.setGraphic(images[this.doorState]);
	HomeObserver.getHomeObserver().updateDoorSensor(this.nameLabel.getText(), this.isOpened);
}
public String getName() {
	return nameLabel.getText();
}
}
