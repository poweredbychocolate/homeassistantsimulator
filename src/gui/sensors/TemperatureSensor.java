package gui.sensors;

import gui.MainApp;
import gui.observer.HomeObserver;
import gui.Icons;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import network.object.NetTemperatureSensor;

public class TemperatureSensor extends GridPane {
	private double temp = 0.0;
	private Label nameLabel, tempLabel, imageLabel;
	private Slider tempSlider;
	private Icons ic;

	public TemperatureSensor(String name, double temp) {
		this.ic = Icons.enableIcons();
		this.setVgap(10.0);
		this.setHgap(10.0);
		this.temp = temp;
		this.tempLabel = new Label(Double.toString(temp) + MainApp.SYMBOLS_CELSIUS);
		this.tempLabel.setTextAlignment(TextAlignment.RIGHT);
		this.nameLabel = new Label(name);
		///
		this.tempSlider = new Slider(0.0, 100.0, temp);
		this.tempSlider.setShowTickLabels(true);
		this.tempSlider.setShowTickMarks(true);
		this.tempSlider.setMajorTickUnit(20);
		this.tempSlider.setMinorTickCount(5);
		this.tempSlider.setBlockIncrement(10);
		this.tempSlider.setEffect(new DropShadow(5, Color.PURPLE));
		///
		this.imageLabel = new Label();
		this.imageLabel.setGraphic(ic.getThermometerIcon());
		///
		this.add(imageLabel, 0, 0, 3, 1);
		this.add(nameLabel, 0, 1, 2, 1);
		this.add(tempLabel, 2, 1);
		this.add(tempSlider, 0, 2, 3, 1);

		///
		this.tempSlider.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				changeTemperature(newValue.doubleValue());

			}
		});
		HomeObserver.getHomeObserver().addTemeperatureSensor(new NetTemperatureSensor(this.getName(),this.getTemperature()));
	}

	public double getTemperature() {
		return this.temp;
	}
	public String getName()
	{
		return nameLabel.getText();
	}

	private void changeTemperature(double val) {
		this.temp = val;
		this.tempLabel.setText(String.format("%3.2f", val) + MainApp.SYMBOLS_CELSIUS);
		this.tempSlider.setValue(val);
		HomeObserver.getHomeObserver().updateTemperatureSensor(getName(), getTemperature());
	}
}
