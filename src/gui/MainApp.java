package gui;

import java.util.ArrayList;
import java.util.HashMap;

import gui.control.LightControl;
import gui.control.RadiatorControl;
import gui.notes.Note;
import gui.observer.HomeObserver;
import gui.sensors.DoorSensor;
import gui.sensors.RainSensor;
import gui.sensors.TemperatureSensor;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import network.Server;

public class MainApp extends Application {
	//// Constant using in aplication
	public static String SYMBOLS_CELSIUS = "\u2103";
	//// Network server
	private Server networkServer;
	// main pane
	private BorderPane mainPane;
	private Scene mainScene;
	/// center pane
	private GridPane centerIconPane;
	private ScrollPane centerIconScrollPane;
	/// note pane
	// private GridPane notePane;
	private VBox notePane;
	private ScrollPane noteScrollPane;
	//// control data sets
	private HashMap<String, LightControl> lightsList;
	private HashMap<String, RadiatorControl> radiatorsList;
	private ArrayList<Note> notesList;
	private final int serverPort=9999;
	@Override
	public void start(Stage primaryStage) {
	   networkServer = new Server(serverPort);
		networkServer.start();
		HomeObserver.getHomeObserver().observeMainApp(this);

		mainPane = new BorderPane();
		////
		initCenterPane();
		initNotePane();
		initTopPane();
		////
		mainScene = new Scene(mainPane, 1024, 720);
		primaryStage.setScene(mainScene);
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				networkServer.shutdownServer();

			}
		});
		// primaryStage.setFullScreen(true);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	private void initCenterPane() {
		centerIconPane = new GridPane();
		centerIconPane.setPadding(new Insets(8));
		fillCenterIconPane();
		centerIconScrollPane = new ScrollPane(centerIconPane);
		centerIconScrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		// mainPane.getChildren().add(centerIconScrollPane);
		mainPane.setCenter(centerIconScrollPane);
	}

	private void fillCenterIconPane() {
		/// temperature sensor
		for (int i = 1; i < 6; i++) {
			centerIconPane.add(new TemperatureSensor("Temp " + i, 10), i, 0);
		}
		/// door sensor
		for (int i = 1; i < 5; i += 2) {
			centerIconPane.add(new DoorSensor("Door " + i, true), i, 1);
			centerIconPane.add(new DoorSensor("Door " + (i + 1), false), i + 1, 1);
		}
		/// rain sensor
		centerIconPane.add(new RainSensor("Rain 1", true), 1, 2);
		centerIconPane.add(new RainSensor("Rain 2", false), 2, 2);
		/// light control
		lightsList = new HashMap<String, LightControl>(6);
		for (int i = 1; i < 7; i++) {
			lightsList.put("Light " + i, new LightControl("Light " + i, false));
			centerIconPane.add(lightsList.get("Light " + i), i, 3);
		}
		/// radiator control
		radiatorsList = new HashMap<String, RadiatorControl>(4);
		for (int i = 1; i < 5; i++) {
			radiatorsList.put("Radiator " + i, new RadiatorControl("Radiator " + i, 20.0 + i));
			centerIconPane.add(radiatorsList.get("Radiator " + i), i, 4);
		}

	}

	public void switchLight(String name) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				lightsList.get(name).switchLightState();
			}
		});
	}

	public void switchRadiator(String name) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				radiatorsList.get(name).changeHeating();
			}
		});
	}

	public void adjustRadiator(String name, double temperature) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				radiatorsList.get(name).changeTemperature(temperature);
			}
		});
	}

	/// since varion 0.2
	/*
	 * private void initNotePane() { notePane = new GridPane(); fillNotePanel();
	 * noteScrollPane = new ScrollPane(notePane);
	 * noteScrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
	 * mainPane.setLeft(noteScrollPane); }
	 * 
	 * private void fillNotePanel() { notesList = new ArrayList<>(10);
	 * notesList.add(new Note("Arduino", "All system online"));
	 * notesList.add(new Note("Arduino", "Welcome to smart home system ")); for
	 * (int i = 0; i < notesList.size(); i++) notePane.add(notesList.get(i),0,
	 * i); }
	 */
	private void initNotePane() {
		notePane = new VBox(5);
		// notePane.setMinWidth(300);
		// notePane.setMaxWidth(300);
		notePane.setPadding(new Insets(5, 5, 5, 5));
		// notePane.setHgap(5);
		// notePane.setAlignment(Pos.TOP_CENTER);;
		fillNotePanel();
		noteScrollPane = new ScrollPane(notePane);
		noteScrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		noteScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		noteScrollPane.setMaxWidth(230);
		noteScrollPane.setMinWidth(230);
		mainPane.setLeft(noteScrollPane);
	}

	private void fillNotePanel() {
		notesList = new ArrayList<>(10);
		addNote("Arduino", "All system online");
		addNote("Arduino", "Welcome to smart home system ");
	    addNote("Example", "Example note 01");
		addNote("Example", "Example note 02");
	}

	public void addNote(String author, String text) {
		Note n = new Note(author, text);
		notesList.add(n);

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				notePane.getChildren().add(0, n);
			}
		});

	}
	private void  initTopPane()
	{
		Label serverLabel = new Label("Smart Home System On Port " +this.serverPort);
		serverLabel.setFont(Font.font("Palatino", 16));
		VBox topBox = new VBox(serverLabel);
		topBox.setAlignment(Pos.CENTER);
		mainPane.setTop(topBox);
	}
}